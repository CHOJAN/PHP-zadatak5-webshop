-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 18, 2018 at 09:01 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `advertise`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id_category`),
  KEY `id_category` (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id_category`, `name`) VALUES
(1, 'Nekretnine'),
(2, 'Automobili'),
(3, 'Računari'),
(4, 'Telefoni'),
(5, 'Poljoprivreda'),
(6, 'Sport i rekreacija');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` varchar(15) NOT NULL,
  `picture` varchar(70) NOT NULL,
  `date` varchar(15) NOT NULL,
  PRIMARY KEY (`id_product`),
  KEY `id_category` (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id_product`, `id_category`, `name`, `description`, `price`, `picture`, `date`) VALUES
(1, 1, 'Dvosoban stan, Subotica, POVOLJNO!', 'Povoljan stan na super lokaciji, Prozivka, Braće Radić 156. Vredi pogledati. ', '30000€', 'stan1.jpg', '17-04-2018'),
(2, 1, 'Subotica-centar', 'Subotica, Centar, prodaje se jednoiposoban komforan stan na trećem spratu površine 58 m2. Sastoji se od spavaće sobe, dnevne sobe, kuhinje sa trpezarijom, kupatila i terase. Grejanje je Toplana, podovi od granita, prozori su PVC. Stan se nalazi u zgradi bez lifta.', '38000€', 'stan2.jpg', '17-04-2018'),
(3, 1, 'Dvosoban stan na Prozivci', 'Prodajem preuređen dvosoban stan na Prozivci u ulici Braće Radić.', '31000€', 'stan3.jpg', '17-04-2018'),
(4, 2, 'Opel Corsa D 1.2 plin', 'Corsa D 1.2 benzin +TNG\r\n\r\nOdličan auto.\r\n\r\nPlin atestiran do 27.12.2022.\r\n\r\nAuto sa dosta opreme.', '3499€', 'auto1.jpg', '17-04-2018'),
(5, 2, 'Hyundai i20 1.2 benzinac', 'Novi i20. U cenu ulazi bela boja, metalik boja se doplaćuje 390 eur.\r\n\r\nGarancija na motor 5+2 godine bez obzira na broj pređenih kilometara.', '9690€', 'auto2.jpg', '17-04-2018'),
(6, 3, 'Minion USB 8GB 2.0', 'Nov USB Minion 8 GB 2.0 usb je od kvalitetne gume/silikon i može se koristiti kao privezak. Slanje post expressom ili licno preuzimanje u BG', '800din', 'comp1.jpg', '17-04-2018'),
(7, 3, 'ASUS Zenbook Pro UX501VW', 'Diagonala ekrana: 39,6 cm (15,6\")\r\nRezolucija: 1920 X 1080\r\nProcesor: Intel i7-6700HQ 2.60 GHz \r\nRAM: 8GB\r\nHDD: 256GB SSD\r\nOperativni sistem: Windows 10\r\nEkran na dodir: Ne\r\nGrafika: NVIDIA GeForce\r\nOpis grafike: NVIDIA GeForce GTX960M 4GB', '885€', 'comp2.jpg', '17-04-2018'),
(8, 4, 'Sony Xperia Z3 Compact D5803', 'Sony Xperia Z3 Compact D5803\r\ncrni, korišćen 65 dana, kao nov.', '179€', 'phone1.jpg', '18-04-2018'),
(9, 4, 'Samsung Galaxy S8', 'Nov Samsung Galaxy S8 (SM-G950) 64GB sim free sa evropskom garancijom u kutiji\r\n\r\nNov Samsung Galaxy S8+ (SM-G955) 64GB sim free sa evropskom garancijom u kutiji 549', '479€', 'phone2.jpg', '18-04-2018'),
(10, 5, 'Plastenici 6x3x2 kvalitetni!!!', 'Kvalitet i računica govore same za sebe.\r\nMontaža je izuzetno jednostavna, po principu uradi sam.\r\n\r\nTehničke karakteristike:\r\nDimenzija 6x3x2m\r\nKonstrukcija od metalnih nerdjajućih čeličnih cevi prečnika 25mm, debljine 0,65mm\r\n140gr UV otporna PE folija sa upredenim ojačanjem \r\n12 roll up prozora sa mrežicom protiv insekata \r\nVrata sa duplim zip-om koja se mogu otvarati iznutra i spolja ', '31000din', 'agro1.jpg', '18-04-2018'),
(11, 5, 'Benzinski trimeri-više modela, novo', 'Više modela benzinskih trimera.\r\n\r\nNovi u fabričkom pakovanju.\r\n\r\nBenziski trimeri.Cene vec od 60 eura\r\n\r\nCMI , rasklopiv , snaga 1,7 ks dodatna oprema CENA 60 EURA\r\n\r\nGARDEN FIELD snaga 3.5 ks CENA 70 EUR.\r\n\r\nStraus 3.5 KS, 023A, oprema 3 alata, cena 70 EUR', 'od 60€', 'agro2.jpg', '18-04-2018'),
(12, 6, 'Hoverbord sa ručkom-električni skuter 10\", šaren', 'Besplatna dostava na teritoriji Srbije!\r\n\r\nKarakteristike:      \r\n- Boja: Sareni HipHop\r\n- Ima ugradjen bluetooth i mogucnost povezivanja sa Android i IPhone telefonima i pustanja muzike\r\n- Tip akumlatora: samsung lithium 36V\r\n- Brzina: oko 20km/h\r\n- Veličina točkova 10″\r\n- Moze da podnese 120kg oterecenja\r\n- Dimenzije: 625mm*286mm*260mm\r\n- Tezina: oko 13kg\r\n\r\n\r\n- Dva svetla sa prednje strane i dva sa zadnje', '169€', 'sport1.jpg', '18-04-2018'),
(13, 6, '4K Ultra HD akciona kamera Wifi', '- Odlična akciono-sportska 4K UltraHD kamera za sjajne video-snimke, fotografije, audio-snimke iz vazduha, snimke pod vodom, u toku exstremnih sportova, sky-divinga, surffinga, biciklizama, auto/motociklizma.\r\n\r\n- Ova kamera ima mogućnost bežičnog spajanja na mobilni i prenos live snimaka (WiFi do 20m).  \r\n\r\n- Predstavlja odličnu alternativu daleko skupljim GoPro kamerama \r\n\r\n', '49€', 'sport2.jpg', '18-04-2018'),
(15, 1, 'kućica', 'mala slatka', '14000€', 'kucica.jpg', '18-04-2018'),
(16, 4, 'HTC 820 G+', 'Dobro očuvan telefon, ispravan u potpunosti, malo koriscen. Uz telefon idu punjac i slusalice. Kontaktirajte ', '90€', 'htc-820-g-5425631591170-71784824983.jpg', '18-04-2018');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`) VALUES
(1, 'admin', '9ee78148dda176224cfed261b121f407'),
(2, 'gost', '55d7e3062c1a2d5cb2e4da57f927f115');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `category` (`id_category`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
